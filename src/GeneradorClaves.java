import java.security.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Base64;

public class GeneradorClaves {
    private String algoritmo;
    public PublicKey publicKey;
    public PrivateKey privateKey;

    public GeneradorClaves(String algoritmo) {
        this.algoritmo = algoritmo;
    }

    public void generar() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance(algoritmo);
            keyGen.initialize(2048);
            KeyPair pair = keyGen.generateKeyPair();
            this.publicKey = pair.getPublic();
            this.privateKey = pair.getPrivate();

            // Guardar las claves en archivos
            guardarClave("publicKey", Base64.getEncoder().encodeToString(publicKey.getEncoded()));
            guardarClave("privateKey", Base64.getEncoder().encodeToString(privateKey.getEncoded()));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void guardarClave(String nombreArchivo, String clave) {
        try (FileOutputStream fos = new FileOutputStream(nombreArchivo)) {
            fos.write(clave.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
