import java.security.*;
import java.util.Scanner;
import javax.crypto.Cipher;
public class CifradoYFirma {
    private String algoritmo;
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private String mensajeNormal;
    private byte[] mensajeCifrado;
    private byte[] firma;

    public CifradoYFirma(String algoritmo, PublicKey publicKey, PrivateKey privateKey) {
        this.algoritmo = algoritmo;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public void cifrarMensaje() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el mensaje a cifrar:");
        this.mensajeNormal = scanner.nextLine();
        try {
            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);
            this.mensajeCifrado = cipher.doFinal(mensajeNormal.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void firmarMensaje() {
        try {
            Signature privateSignature = Signature.getInstance("SHA256withRSA");
            privateSignature.initSign(privateKey);
            privateSignature.update(mensajeNormal.getBytes());
            this.firma = privateSignature.sign();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public byte[] getMensajeCifrado() {
        return mensajeCifrado;
    }

    public byte[] getFirma() {
        return firma;
    }
}